FROM tomcat:8.0
MAINTAINER pabloandi@gmail.com

RUN apt-get update && apt-get install -y locales && rm -rf /var/lib/apt/lists/* \
	&& localedef -i es_CO -c -f UTF-8 -A /usr/share/locale/locale.alias es_CO.UTF-8

ENV LANG es_CO.utf8
ENV LANGUAGE es_CO:es
ENV LC_ALL es_CO.UTF-8 

ADD tomcat-users.xml /usr/local/tomcat/conf/tomcat-users.xml

EXPOSE 8080

# Set the default command to run when starting the container
CMD ["catalina.sh", "run"]
